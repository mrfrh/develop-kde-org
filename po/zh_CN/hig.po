msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-04-25 00:38+0000\n"
"PO-Revision-Date: 2022-12-24 11:42\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"SPDX-FileCopyrightText: FULL NAME <EMAIL@ADDRESS>\n"
"SPDX-License-Identifier: LGPL-3.0-or-later\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-develop-kde-org/hig.pot\n"
"X-Crowdin-File-ID: 26515\n"

#: content/hig/_index.md:0
msgid "KDE Human Interface Guidelines"
msgstr "KDE 人机界面指南"

#: content/hig/_index.md:9
msgid ""
"The KDE Human Interface Guidelines (HIG) offer designers and developers a "
"set of recommendations for producing beautiful, usable, and consistent user "
"interfaces for convergent desktop and mobile applications and workspace "
"widgets. Their aim is to improve the experience for users by making "
"application and widget interfaces more consistent and hence more intuitive "
"and learnable."
msgstr ""

#: content/hig/_index.md:16
msgid ""
"![Simple by default, powerful when needed.](/hig/HIGDesignVisionFullBleed."
"png)"
msgstr ""

#: content/hig/_index.md:18
msgid "Design Vision"
msgstr "设计愿景"

#: content/hig/_index.md:21
msgid ""
"KDE's design vision focuses on two attributes of KDE software that connect "
"its future to its history:"
msgstr ""

#: content/hig/_index.md:24
msgid "Simple by default..."
msgstr "默认状态简洁易用..."

#: content/hig/_index.md:26
msgid ""
"*Simple and inviting. KDE software is pleasant to experience and easy to use."
"*"
msgstr ""

#: content/hig/_index.md:29
msgid ""
"**Make it easy to focus on what matters** --- Remove or minimize elements "
"not crucial to the primary or main task. Use spacing to keep things "
"organized. Use color to draw attention. Reveal additional information or "
"optional functions only when needed."
msgstr ""

#: content/hig/_index.md:33
msgid ""
"**I know how to do that!** --- Make things easier to learn by reusing design "
"patterns from other applications. Other applications that use good design "
"are a precedent to follow."
msgstr ""

#: content/hig/_index.md:36
msgid ""
"**Do the heavy lifting for me** --- Make complex tasks simple. Make novices "
"feel like experts. Create ways in which your users can naturally feel "
"empowered by your software."
msgstr ""

#: content/hig/_index.md:40
msgid "...Powerful when needed"
msgstr ""

#: content/hig/_index.md:42
msgid ""
"*Power and flexibility. KDE software allows users to be effortlessly "
"creative and efficiently productive.*"
msgstr ""

#: content/hig/_index.md:45
msgid ""
"**Solve a problem** --- Identify and make very clear to the user what need "
"is addressed and how."
msgstr ""

#: content/hig/_index.md:47
msgid ""
"**Always in control** --- It should always be clear what can be done, what "
"is currently happening, and what has just happened. The user should never "
"feel at the mercy of the tool. Give the user the final say."
msgstr ""

#: content/hig/_index.md:51
msgid ""
"**Be flexible** --- Provide sensible defaults but consider optional "
"functionality and customization options that don\\'t interfere with the "
"primary task."
msgstr ""

#: content/hig/_index.md:55
msgid "Note"
msgstr ""

#: content/hig/_index.md:57
msgid ""
"KDE encourages developing and designing for customization, while providing "
"good default settings. Integrating into other desktop environments is also a "
"virtue, but ultimately we aim for perfection within our own Plasma desktop "
"environment with the default themes and settings. This aim should not be "
"compromised."
msgstr ""
"KDE 鼓励为定制开发和设计，同时提供良好的默认设置。 融入其他桌面环境也是一种美"
"德， 但最终我们的目标是在我们自己的 Plasma 桌面环境中完善默认主题和设置。 这"
"一目标不应受到损害。"
