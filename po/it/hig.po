#
# Vincenzo Reale <smart2128vr@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: documentation-develop-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-04-25 00:38+0000\n"
"PO-Revision-Date: 2022-01-30 16:47+0100\n"
"Last-Translator: Vincenzo Reale <smart2128vr@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.1\n"

#: content/hig/_index.md:0
msgid "KDE Human Interface Guidelines"
msgstr "Linee guida di interfaccia umana di KDE"

#: content/hig/_index.md:9
msgid ""
"The KDE Human Interface Guidelines (HIG) offer designers and developers a "
"set of recommendations for producing beautiful, usable, and consistent user "
"interfaces for convergent desktop and mobile applications and workspace "
"widgets. Their aim is to improve the experience for users by making "
"application and widget interfaces more consistent and hence more intuitive "
"and learnable."
msgstr ""
"Le linee guida per l'interfaccia umana (HIG) di KDE offrono a progettisti e "
"sviluppatori una serie di consigli per la produzione di interfacce utente "
"belle, utilizzabili e coerenti per applicazioni desktop e mobili convergenti "
"e oggetti dell'area di lavoro. Il loro scopo è migliorare l'esperienza degli "
"utenti rendendo le interfacce di applicazioni e oggetti più coerenti e "
"quindi più intuitive e di facile apprendimento."

#: content/hig/_index.md:16
msgid ""
"![Simple by default, powerful when needed.](/hig/HIGDesignVisionFullBleed."
"png)"
msgstr ""
"![Semplice per scelta, potente quando server.](/hig/HIGDesignVisionFullBleed."
"png)"

#: content/hig/_index.md:18
msgid "Design Vision"
msgstr "Visione progettuale"

#: content/hig/_index.md:21
msgid ""
"KDE's design vision focuses on two attributes of KDE software that connect "
"its future to its history:"
msgstr ""
"La visione progettuale di KDE si concentra su due attributi del software KDE "
"che collegano il suo futuro alla sua storia:"

#: content/hig/_index.md:24
msgid "Simple by default..."
msgstr "Semplice per scelta..."

#: content/hig/_index.md:26
msgid ""
"*Simple and inviting. KDE software is pleasant to experience and easy to use."
"*"
msgstr ""
"*Semplice e invitante. Il software KDE è piacevole da provare e facile da "
"usare.*"

#: content/hig/_index.md:29
msgid ""
"**Make it easy to focus on what matters** --- Remove or minimize elements "
"not crucial to the primary or main task. Use spacing to keep things "
"organized. Use color to draw attention. Reveal additional information or "
"optional functions only when needed."
msgstr ""
"**Semplifica la concentrazione su ciò che conta** --- Rimuovi o riduci al "
"minimo gli elementi non cruciali per l'attività principale. Usa la "
"spaziatura per mantenere le cose organizzate. Usa il colore per attirare "
"l'attenzione. Rivela informazioni aggiuntive o funzioni opzionali solo "
"quando necessario."

#: content/hig/_index.md:33
msgid ""
"**I know how to do that!** --- Make things easier to learn by reusing design "
"patterns from other applications. Other applications that use good design "
"are a precedent to follow."
msgstr ""
"**So come farlo!** --- Semplifica l'apprendimento riutilizzando i modelli di "
"progettazione di altre applicazioni. Altre applicazioni che utilizzano ben "
"progettate sono un precedente da seguire."

#: content/hig/_index.md:36
msgid ""
"**Do the heavy lifting for me** --- Make complex tasks simple. Make novices "
"feel like experts. Create ways in which your users can naturally feel "
"empowered by your software."
msgstr ""
"**Fai il lavoro pesante per me** --- Semplifica le attività complesse. Fai "
"sentire i principianti degli esperti. Crea modi in cui i tuoi utenti possono "
"sentirsi naturalmente potenziati dal tuo software."

#: content/hig/_index.md:40
msgid "...Powerful when needed"
msgstr "...Potente quando serve"

#: content/hig/_index.md:42
msgid ""
"*Power and flexibility. KDE software allows users to be effortlessly "
"creative and efficiently productive.*"
msgstr ""
"*Potenza e flessibilità. Il software KDE consente agli utenti di essere "
"creativi senza sforzo ed efficientemente produttivi.*"

#: content/hig/_index.md:45
msgid ""
"**Solve a problem** --- Identify and make very clear to the user what need "
"is addressed and how."
msgstr ""
"**Risolvi un problema** --- Identificare e chiarire all'utente quale "
"necessità viene affrontata e come."

#: content/hig/_index.md:47
msgid ""
"**Always in control** --- It should always be clear what can be done, what "
"is currently happening, and what has just happened. The user should never "
"feel at the mercy of the tool. Give the user the final say."
msgstr ""
"**Sempre sotto controllo** --- Dovrebbe essere sempre chiaro cosa si può "
"fare, cosa sta succedendo e cosa è appena successo. L'utente non dovrebbe "
"mai sentirsi in balia dello strumento. Dai all'utente l'ultima parola."

#: content/hig/_index.md:51
msgid ""
"**Be flexible** --- Provide sensible defaults but consider optional "
"functionality and customization options that don\\'t interfere with the "
"primary task."
msgstr ""
"**Sii flessibile** --- Fornisci impostazioni predefinite ragionevoli, ma "
"considera funzionalità opzionali e opzioni di personalizzazione che non "
"interferiscono con l'attività principale."

#: content/hig/_index.md:55
msgid "Note"
msgstr "Nota"

#: content/hig/_index.md:57
msgid ""
"KDE encourages developing and designing for customization, while providing "
"good default settings. Integrating into other desktop environments is also a "
"virtue, but ultimately we aim for perfection within our own Plasma desktop "
"environment with the default themes and settings. This aim should not be "
"compromised."
msgstr ""
"KDE incoraggia lo sviluppo e la progettazione per la personalizzazione, "
"fornendo al contempo buone impostazioni predefinite. Anche l'integrazione in "
"altri ambienti desktop è una virtù, ma alla fine miriamo alla perfezione "
"all'interno del nostro ambiente desktop Plasma con i temi e le impostazioni "
"predefiniti. Questo obiettivo non deve essere compromesso."
